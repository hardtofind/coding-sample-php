# Hardtofind PHP Coding Example #

Using [composer](https://getcomposer.org/), bring the [symfony/event-dispatcher](http://symfony.com/doc/current/components/event_dispatcher.html) component into a simple PHP script. The script must construct and dispatch a [GenericEvent](http://symfony.com/doc/current/components/event_dispatcher/generic_event.html) using an event subscriber.

The event should log the date and time to a file each time it is triggered.

Please structure your code to composer autoload.

Example output:

```
user@app:~$ php index.php
Event triggered at: 2017-05-04T02:21:24+0000
user@app:~$ php index.php
Event triggered at: 2017-05-04T02:21:27+0000
```

Please submit your code as a link to a hosted git repository to your recruitment contact.